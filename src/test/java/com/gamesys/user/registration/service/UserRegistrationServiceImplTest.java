package com.gamesys.user.registration.service;

import com.gamesys.user.registration.exception.ExclusionDataException;
import com.gamesys.user.registration.exception.UserNotFoundException;
import com.gamesys.user.registration.model.User;
import com.gamesys.user.registration.repository.UserRegistrationRepository;
import com.gamesys.user.registration.request.UserRegistrationRequestDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserRegistrationServiceImpl.class)
public class UserRegistrationServiceImplTest {

    @MockBean
    private UserRegistrationRepository userRegistrationRepository;

    @MockBean
    private ExclusionService exclusionService;

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterUser() {
        User user = setUser();
        when(userRegistrationRepository.saveAndFlush(user)).thenReturn(user);
        when(exclusionService.validate(anyString(), anyString())).thenReturn(true);

        userRegistrationService.registerUser(setUserRegistrationRequestDto());

        verify(userRegistrationRepository, times(1)).saveAndFlush(Mockito.any(User.class));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testRegisterUserShouldThrowDataIntegrityViolationException() {
        when(exclusionService.validate(anyString(), anyString())).thenReturn(Boolean.TRUE);
        when(userRegistrationRepository.existsById(anyString())).thenReturn(Boolean.TRUE);

        userRegistrationService.registerUser(setUserRegistrationRequestDto());
    }

    @Test(expected = ExclusionDataException.class)
    public void testRegisterUserShouldThrowExclusionDataException() {
        when(exclusionService.validate(anyString(), anyString())).thenReturn(Boolean.FALSE);
        when(userRegistrationRepository.existsById(anyString())).thenReturn(Boolean.TRUE);

        userRegistrationService.registerUser(setUserRegistrationRequestDto());
    }

    @Test
    public void testGetAllUsers() {
        User user = setUser();
        when(userRegistrationRepository.findAll()).thenReturn(Arrays.asList(user));

        List<User> userResult = userRegistrationService.getAllUser();
        assertEquals(1, userResult.size());
        assertEquals(user.getUsername(), userResult.get(0).getUsername());
    }

    @Test
    public void testGetUserBySSN() {
        User user = setUser();

        when(userRegistrationRepository.findById(user.getSsn())).thenReturn(Optional.of(user));
        Optional<User> userData = userRegistrationService.getUserBySSN(user.getSsn());
        assertTrue(userData.isPresent());
        assertEquals(user.getUsername(), userData.get().getUsername());
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUserBySSNShouldThrowUserNotFoundException() {
        User user = setUser();
        when(userRegistrationRepository.findById(user.getSsn())).thenReturn(Optional.empty());
        userRegistrationService.getUserBySSN(user.getSsn());
    }

    private User setUser() {
        User user = new User();
        user.setUsername("AmitKumar123");
        user.setSsn("123456789");
        user.setPassword("Welcome123");
        user.setDateOfBirth(LocalDate.now().minusYears(1));
        return user;
    }

    private UserRegistrationRequestDto setUserRegistrationRequestDto() {
        UserRegistrationRequestDto userRegistrationRequestDto = new UserRegistrationRequestDto();
        userRegistrationRequestDto.setUsername("AmitKumar123");
        userRegistrationRequestDto.setPassword("Welcome123");
        userRegistrationRequestDto.setSsn("123456789");
        userRegistrationRequestDto.setDateOfBirth(LocalDate.now().minusYears(1));
        return userRegistrationRequestDto;
    }

}