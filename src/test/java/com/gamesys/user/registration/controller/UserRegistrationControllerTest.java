package com.gamesys.user.registration.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesys.user.registration.exception.ExclusionDataException;
import com.gamesys.user.registration.model.User;
import com.gamesys.user.registration.request.UserRegistrationRequestDto;
import com.gamesys.user.registration.service.UserRegistrationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserRegistrationController.class)
public class UserRegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserRegistrationService userRegistrationService;

    @Test
    public void testRegisterUserWhenValidUrlAndMethodAndContentType_thenReturns201() throws Exception {

        mockMvc.perform(post("/user/register")
                .content(objectMapper.writeValueAsString(getUserRegistrationRequestDto()))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void testRegisterUserWhenValidUrlAndUserInExclusionList_thenThrowExclusionDataException() throws Exception {
        doThrow(ExclusionDataException.class).when(userRegistrationService).registerUser(ArgumentMatchers.any(UserRegistrationRequestDto.class));

        mockMvc.perform(post("/user/register")
                .content(objectMapper.writeValueAsString(getUserRegistrationRequestDto()))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testRegisterUserWhenValidUrlAndMethodAndUserAlreadyRegistered_thenThrowDataIntegrityViolationException() throws Exception {
        doThrow(DataIntegrityViolationException.class).when(userRegistrationService).registerUser(ArgumentMatchers.any(UserRegistrationRequestDto.class));

        mockMvc.perform(post("/user/register")
                .content(objectMapper.writeValueAsString(getUserRegistrationRequestDto()))
                .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetAllUserWhenValidUrlAndMethodAndContentType_thenReturns200() throws Exception {
        List<User> usersList = Arrays.asList(getUser());
        when(userRegistrationService.getAllUser()).thenReturn(usersList);

        mockMvc.perform(get("/user")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andExpect(jsonPath("$[0].ssn", is(usersList.get(0).getSsn())))
                .andExpect(jsonPath("$[0].username", is(usersList.get(0).getUsername())));
    }

    @Test
    public void testGetUserBySSNWhenValidUrlAndMethodAndContentType_thenReturns200() throws Exception {
        User user = getUser();
        when(userRegistrationService.getUserBySSN(user.getSsn())).thenReturn(Optional.of(user));

        mockMvc.perform(get("/user/{ssn}", "123456789")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ssn", is(user.getSsn())))
                .andExpect(jsonPath("$.username", is(user.getUsername())));
    }

    private User getUser() {
        User user = new User();
        user.setUsername("AmitKumar123");
        user.setSsn("123456789");
        user.setPassword("Welcome123");
        user.setDateOfBirth(LocalDate.parse("1912-06-23"));
        return user;
    }

    private UserRegistrationRequestDto getUserRegistrationRequestDto() {
        UserRegistrationRequestDto userRegistrationRequestDto = new UserRegistrationRequestDto();
        userRegistrationRequestDto.setUsername("AmitKumar123");
        userRegistrationRequestDto.setSsn("123456789");
        userRegistrationRequestDto.setPassword("Welcome123");
        userRegistrationRequestDto.setDateOfBirth(LocalDate.parse("1912-06-23"));
        return userRegistrationRequestDto;
    }

}