package com.gamesys.user.registration.controller;

import com.gamesys.user.registration.model.User;
import com.gamesys.user.registration.request.UserRegistrationRequestDto;
import com.gamesys.user.registration.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class UserRegistrationController {

    @Autowired
    private UserRegistrationService userRegistrationService;

    @PostMapping("/user/register")
    public ResponseEntity registerUser(@Valid @RequestBody UserRegistrationRequestDto userRegistrationRequestDto) {
        userRegistrationService.registerUser(userRegistrationRequestDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/user/{ssn}")
    public Optional<User> getUserBySSN(@Valid @PathVariable String ssn) {
        return userRegistrationService.getUserBySSN(ssn);
    }

    @GetMapping("/user")
    public List<User> getAllUser() {
        return userRegistrationService.getAllUser();
    }
}