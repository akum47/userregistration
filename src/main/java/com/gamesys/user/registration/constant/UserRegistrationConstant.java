package com.gamesys.user.registration.constant;

public class UserRegistrationConstant {

    public static final String USERNAME_NOT_BLANK = "Username should not be blank";
    public static final String USERNAME_POLICY = "Your username should be alphanumerical with no spaces";
    public static final String USERNAME_REGEX = "^[a-zA-Z0-9]*$";
    public static final String PASSWORD_NOT_BLANK = "Password should not be blank";
    public static final String PASSWORD_POLICY = "Your password must be at least 4 characters long, contain at least one lower case character, at least one" +
            "upper case character and at least one number";
    public static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,}$";
    public static final String DATE_OF_BIRTH_POLICY = "Invalid date of birth";
    public static final String SSN_NOT_BLANK = "SSN should not be blank";
    public static final String USER_ALREADY_REGISTERED = "This user is already registered";
    public static final String USER_IN_EXCLUSION_LIST = "Registration is not possible because user is in exclusion list";
    public static final String USER_NOT_FOUND_WITH_SSN = "User not found with SSN - ";

}