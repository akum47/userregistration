package com.gamesys.user.registration.request;

import com.gamesys.user.registration.constant.UserRegistrationConstant;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

public class UserRegistrationRequestDto {

    @NotBlank(message = UserRegistrationConstant.USERNAME_NOT_BLANK)
    @Pattern(regexp = UserRegistrationConstant.USERNAME_REGEX, message = UserRegistrationConstant.USERNAME_POLICY)
    private String username;

    @NotBlank(message = UserRegistrationConstant.PASSWORD_NOT_BLANK)
    @Pattern(regexp = UserRegistrationConstant.PASSWORD_REGEX, message = UserRegistrationConstant.PASSWORD_POLICY)
    private String password;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Past(message = UserRegistrationConstant.DATE_OF_BIRTH_POLICY)
    private LocalDate dateOfBirth;

    @NotBlank(message = UserRegistrationConstant.SSN_NOT_BLANK)
    private String ssn;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }
}
