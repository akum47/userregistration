package com.gamesys.user.registration.service;

import com.gamesys.user.registration.model.User;
import com.gamesys.user.registration.request.UserRegistrationRequestDto;

import java.util.List;
import java.util.Optional;


public interface UserRegistrationService {

    void registerUser(UserRegistrationRequestDto userRegistrationRequest);

    Optional<User> getUserBySSN(String ssn);

    List<User> getAllUser();
}
