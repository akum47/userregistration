package com.gamesys.user.registration.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExclusionServiceImpl implements ExclusionService {

    @Override
    public boolean validate(String dateOfBirth, String ssn) {
        ExclusionData exclusionData = createExclusionList().stream().
                filter(item -> dateOfBirth.equals(item.dob) && ssn.equals(item.ssn))
                .findAny()
                .orElse(null);

        return null == exclusionData;
    }

    private List<ExclusionData> createExclusionList() {
        List<ExclusionData> exclusionDataList = new ArrayList<>();
        exclusionDataList.add(new ExclusionData("1815-12-10", "85385075"));
        exclusionDataList.add(new ExclusionData("1912-06-23", "123456789"));
        exclusionDataList.add(new ExclusionData("1910-06-22", "987654321"));
        return exclusionDataList;
    }

    class ExclusionData {
        String dob;
        String ssn;

        ExclusionData(String dob, String ssn) {
            this.dob = dob;
            this.ssn = ssn;

        }
    }
}