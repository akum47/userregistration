package com.gamesys.user.registration.service;

import com.gamesys.user.registration.constant.UserRegistrationConstant;
import com.gamesys.user.registration.exception.ExclusionDataException;
import com.gamesys.user.registration.exception.UserNotFoundException;
import com.gamesys.user.registration.model.User;
import com.gamesys.user.registration.repository.UserRegistrationRepository;
import com.gamesys.user.registration.request.UserRegistrationRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

    @Autowired
    private UserRegistrationRepository userRegistrationRepository;

    @Autowired
    private ExclusionService exclusionService;

    @Override
    public void registerUser(UserRegistrationRequestDto userRegistrationRequest) {

        if (!exclusionService.validate(userRegistrationRequest.getDateOfBirth().toString(), userRegistrationRequest.getSsn())) {
            throw new ExclusionDataException(UserRegistrationConstant.USER_IN_EXCLUSION_LIST);
        }

        if (userRegistrationRepository.existsById(userRegistrationRequest.getSsn())) {
            throw new DataIntegrityViolationException(UserRegistrationConstant.USER_ALREADY_REGISTERED);
        }

        User user = new User();
        user.setDateOfBirth(userRegistrationRequest.getDateOfBirth());
        user.setPassword(userRegistrationRequest.getPassword());
        user.setSsn(userRegistrationRequest.getSsn());
        user.setUsername(userRegistrationRequest.getUsername());

        userRegistrationRepository.saveAndFlush(user);
    }

    public List<User> getAllUser() {
        return userRegistrationRepository.findAll();
    }

    public Optional<User> getUserBySSN(String ssn) {

        Optional<User> user = userRegistrationRepository.findById(ssn);

        if (!user.isPresent()) {
            throw new UserNotFoundException(UserRegistrationConstant.USER_NOT_FOUND_WITH_SSN + ssn);
        }

        return user;
    }
}
