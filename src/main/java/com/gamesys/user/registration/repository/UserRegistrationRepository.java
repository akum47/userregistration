package com.gamesys.user.registration.repository;

import com.gamesys.user.registration.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRegistrationRepository extends JpaRepository<User, String> {
}
