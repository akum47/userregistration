##User Registration with Exclusion Services

####Case study

This is an application that offers a remote service interface (endpoint) to register a user.

####Condition

A user can only register if not part of an exclusion list and shouldn't have already registered previously.

####Development

The project is implemented using Java 8, Spring Boot as WebServer, JPA, REST as the remote service protocol, Maven build tool, Junit for unit test cases and Mockito for stubbing.

####Functionality

1. User can register
2. Admin can view all the users and view specific user based on the SSN. These endpoints are developed just to check if user exists in the system.

####How to build and run the application:

````
Execute the start.sh script
````

####Curl command to successfully register a user

````
curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d '{
	"username":"AmitKumar123",
	"password":"Welcome123",
	"dateOfBirth":"1990-11-05",
	"ssn":"135724430"
}' "http://localhost:8080/user/register"

````

####Endpoints Exposed:

To register a new user


    Create(POST): /user/register

For Admin to view all the users and view specific user based on the SSN.

````
View specific user by SSN(GET): /user/{ssn}

View all the users(GET): /user
````

####Assumption

Social Security Number(SSN) is not being validated in the code with the real format(AAA-GG-SSSS) as provided in USA, it accepts any string.